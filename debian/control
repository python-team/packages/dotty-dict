Source: dotty-dict
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Edward Betts <edward@4angle.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-sequence-sphinxdoc,
               pybuild-plugin-pyproject,
               python3-all,
               python3-poetry-core,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-sphinx,
               python3-sphinx-rtd-theme
Rules-Requires-Root: no
Standards-Version: 4.6.1
Homepage: https://github.com/pawelzny/dotty_dict
Vcs-Browser: https://salsa.debian.org/python-team/packages/dotty-dict
Vcs-Git: https://salsa.debian.org/python-team/packages/dotty-dict.git

Package: python3-dotty-dict
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: Dictionary wrapper for quick access to deeply nested keys
 Features:
 .
  * Simple wrapper around Python dictionary and dict like objects
  * Two wrappers with the same dict are considered equal
  * Access to deeply nested keys with dot notation: dot['deeply.nested.key']
  * Create, read, update and delete nested keys of any length
  * Access dicts in lists by index dot['parents.0.first_name']
  * Support for setting value in multidimensional lists
  * Support for accessing lists with slices
